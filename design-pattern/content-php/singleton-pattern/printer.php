<?php

class Printer {
    private static $printer;
    private $nrOfPages = 0;

    private function __construct() {
    }

    public static function getInstance() {
        return self::$printer === null ?
            self::$printer = new Printer() :
            self::$printer;
    }

    public function print($text) {
        echo $text .
            "\n Pages printed today: " . ++$this->nrOfPages ."<br>";
    }
}

// Uso del Singleton Printer
//$printer1 = Printer::getInstance();
//$printer2 = Printer::getInstance();

//$printer1->print("Documento 1");
//$printer2->print("Documento 2");

?>
