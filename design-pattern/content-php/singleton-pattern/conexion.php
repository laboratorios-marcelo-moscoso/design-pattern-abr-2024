<?php

class Conexion {
    // Declaración
    private static $instancia;

    //evita la instancia mediante el operador "new"
    private function __construct() {
    }

    //obteniene la instancia únicamente a través de este método
    //palabra reservada "static" permite el acceso mediante Clase::método
    public static function getInstancia() {
        if (self::$instancia == null) {
            self::$instancia = new Conexion();
        }
        return self::$instancia;
    }

    // Método de prueba
    public function conectar() {
        echo "<p>Se conecto a la ref. en memoria de la DB</p>";
    }

    // Método de prueba
    public function desconectar() {
        echo "<p class='w3-text-red'>Se desconecto de la ref en memoria de la DB</p>";
    }
}

// Uso de la clase Conexion
echo "<h3>Coneccion 1</h3>";
$conexion1 = Conexion::getInstancia();
$conexion1->conectar();
$conexion1->desconectar();

echo "<h3>Coneccion 2</h3>";
$conexion2 = Conexion::getInstancia();
$conexion2->conectar();
$conexion2->desconectar();

?>
