<?php

require_once 'printer.php';

class Employee {
    private $name;
    private $role;
    private $assignment;

    public function __construct($name, $role, $assignment) {
        $this->name = $name;
        $this->role = $role;
        $this->assignment = $assignment;
    }

    public function printCurrentAssignment() {
        $printer = Printer::getInstance();
        $printer->print("Employee: " . $this->name . "\n" .
            "Role: " . $this->role . "\n" .
            "Assignment: " . $this->assignment . "\n");
    }
}

// Uso del Singleton Printer
$employee1 = new Employee("John", "Manager", "Project X");
$employee1->printCurrentAssignment();

$employee2 = new Employee("Alice", "Developer", "Project Y");
$employee2->printCurrentAssignment();

?>
