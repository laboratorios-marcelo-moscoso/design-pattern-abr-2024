<?php
class Singleton
{
    private static $instance;
    // El constructor es privado para evitar que se pueda instanciar la clase directamente.
    private function __construct()
    {
        // Puedes inicializar aquí cualquier estado que necesite la instancia.
    }
    // Método estático para obtener la instancia única de la clase.
    public static function getInstance()
    {
        // Si la instancia aún no ha sido creada, la creamos.
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        // Devolvemos la instancia única.
        return self::$instance;
    }
    // Ejemplo de un método de instancia.
    public function someMethod()
    {
        return "Esto es un método de la instancia del Singleton.";
    }
}
// Uso del Singleton
$instance1 = Singleton::getInstance();
$instance2 = Singleton::getInstance();
// Comprobamos que $instance1 y $instance2 son la misma instancia.
var_dump($instance1 === $instance2); // Devuelve true
// Llamamos a un método de la instancia.
echo $instance1->someMethod(); // Imprime: Esto es un método de la instancia del Singleton.
?>
