public class Conexion {
	
	//Declaración
	private static Conexion instancia;
	
	//evita instancia mediante operador "new"
	private Conexion() {
		
	}
	
	//Para obtener la instancia unicamente por este metodo
	//Notese la palabra reservada "static" hace posible el acceso mediante Clase.metodo
	public static Conexion getInstancia() {
		if(instancia == null) {
			instancia = new Conexion();
		}
		return instancia;
	}
	
	//Método de prueba
	public void conectar() {
		System.out.println("Se conecto a la ref. en memoria de la DB");
	}
	
	//Método de prueba
	public void desconectar() {
		System.out.println("Se desconecto de la ref en memoria de la DB");
	}

}